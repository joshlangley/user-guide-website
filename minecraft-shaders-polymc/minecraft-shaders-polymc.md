---
layout: page
title:  "Running Minecraft Shaders through PolyMC"
author: Joshua Langley
author: Aiden Baugh
author: Robert Raulerson
categories: user-guides
permalink: /minecraft-shaders-polymc/
---

# Running Minecraft Shaders through PolyMC

Installing shaders at first may seem like a frightening endeavor, however, with our guide we hope to allow you to get past this stigma. With this guide, you will easily get into being able to play Minecraft and have it look more pleasing to the eye rather than the 32x32 blocks you have grown accustomed to seeing in the vanilla lighting.

<img src="./banner.png" alt="Article Banner" width="100%">

## Requirements

- A valid Microsoft account with Minecraft purchased
- The PolyMC launcher (must be installed on your computer, you can find a guide [here](https://polymc.org/wiki/getting-started/installing-polymc/))
- PolyMC must be signed into the above Microsoft account (a guide can be found [here](TODO))
- A Java Runtime Environment (JRE) installed system-wide (a guide can be found [here](https://polymc.org/wiki/getting-started/installing-java/))
- An Optifine-style shader .zip file to use
    - Some good ones to try out are [Sildur’s Shaders](https://sildurs-shaders.github.io/) and the [SEUS Shaders](https://www.sonicether.com/seus/)
    - Iris keeps a [full list of supported shaders](https://github.com/IrisShaders/Iris/blob/trunk/docs/supportedshaders.md). Some shaders not on this list may work, but with unpredictable results.

    *Warning: When downloading Minecraft mod files, it is common to end up on an adgaurd, adfly or adfocus page. If you end up on one of these pages, DO NOT click anything except the “Skip” button in the top right of the page! Doing so can install viruses or put you in the wrong location.

You will also need to be familiar with the basics of navigating your computer.

## Steps

### Step 1: Creating an Instance

<img src="./step1.gif" alt="Step 1 animated GIF" width="100%">

Assuming you have PolyMC open click the "Add Instance". (Think of it as a certain profile for Minecraft to run mods)

1. Name the instance to whatever you wish
1. Click “Fabric” under the Mod Loader menu (different services to load mods)
1. Select “OK”

### Step 2: Adding and Installing the mods to the Instance

<img src="./step2.gif" alt="Step 2 animated GIF" width="100%">

Once you are back to the PolyMC main window, select the option to “Edit Instance.” (Or press `CTRL + I`)

1. Navigate to the “Mods” tab on the left.
1. Select “Download mods” button on the right
1. There are five mods to individually add to a list which will all be downloaded at once.
    1. Fabric API (Application Programming Interface) - Allows mods to interface with the game using the Fabric mod loader.
    1. Sodium - Rendering engine optimizer.
    1. Lithium - Game logic optimizer.
    1. Iris Shaders - Loads OptiFine shader packs.
    1. Starlight (Fabric) - Optimizes lighting in the game.
1. Select one of the mods and click “Select mod for download” at the bottom right of the window. (This places it into the list to download). Repeat this for all five mods.

    TIP: You can use the search bar at the top and type in the name to find it quicker.

1. Select “OK” and all five mods will download at once.
1. A new window will open, showing the list of mods to download. Confirm that all five mods are listed, then choose “OK” again.

### Step 3: Adding Shader Packs to the Instance

<img src="./step3.gif" alt="Step 3 animated GIF" width="100%">

Navigate to shader packs via the left menu.

1. Choose “Add” at the top right.
1. Navigate to the shader `.zip` file that you wish to have installed in Minecraft.
    1. Ensure you know where you downloaded the shader file as well as what it's called. More than likely, it is in your download section.
    1. Ensure the check box is ticked on the left side of its title.
1. Select “Launch” at the bottom right.

### Step 4: Generate a World

<img src="./launch-mc-and-world-gen.gif" alt="Launching Minecraft and Generating a World animated GIF" width="100%">

Once the game loads, select “Singleplayer”, then give the world an appropriate name and choose “Create New World”.

*Note: When you enter a world for the first time, Minecraft will present you with tutorials in the upper right, but these can block buttons in later steps. To clear these, just follow the instructions they provide.

<img src="./instructions-blocking-removal.gif" alt="Clearing tutorials blocking buttons animated GIF" width="100%">

### Step 4: Running the Shaders in Game

<img src="./step4.gif" alt="Step 4 animated GIF" width="100%">

Once you are in the game click `ESC` on your keyboard to get into the Game Menu.

1. Select “Options”
1. Click on “Video Settings”
1. At the top right select “Shader packs”
1. Double click the shader pack you wish to activate.
1. At the bottom middle of the screen select “apply”

TIP: If you have a powerful enough computer, you can take this a step further by adding a realistic resource pack to make blocks look more realistic.
