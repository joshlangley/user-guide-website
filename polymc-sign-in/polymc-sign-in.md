---
layout: page
title:  "Signing into PolyMC"
author: Aiden Baugh
author: Joshua Langley
categories: user-guides
permalink: /polymc-sign-in/
---

# Signing into PolyMC

Messing with your computer's files at first may seem like a frightening endeavor, however, with our guide we hope to allow you to get past this stigma. With this guide, you will easily get into being able to play Minecraft and have it look more pleasing to the eye rather than the 32x32 blocks you’ve grown accustomed to seeing in the vanilla lighting.

## Requirements

- A valid Microsoft account with Minecraft purchased
- The PolyMC launcher (must be installed on your computer, you can find a guide [here](https://polymc.org/wiki/getting-started/installing-polymc/))

You will also need to be familiar with the basics of navigating your computer.

## Steps

<img src="./signin1.gif" alt="GIF depicting the steps in action" width="100%">

1. From the PolyMC main window, choose "Profiles" the upper right, then "Manage Accounts...".
1. Once you are in the accounts screen you will need to navigate to the upper right corner of the screen and click on "Add Microsoft".
1. After you click “Add Microsoft” a screen will appear. Once that screen appears click “Open page and copy code”.
1. The previous step will open your web browser to a Microsoft page telling you to enter a code. Click on the box below Enter Code and right click and paste (or press `CTRL + V`) to paste the code into the box and then, click “Next” once you paste in the code.
1. The next page is the Microsoft account sign in. If you are already signed in, you can skip this step.

If you see the below dialog you can close the browser and PolyMC Settings window and are good to go.

<img src="./signin-confirmation-dialog.png" alt="You're now signed in to PolyMC. You can now safely close this window." style="max-width:459px; width: 100%;">

Have fun playing Minecraft!
