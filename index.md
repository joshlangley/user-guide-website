---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title:  "Home"
---

# Welcome

*We build better gaming resources for a better future*

## Tutorials

- [Running Minecraft Shaders through PolyMC](/user-guide-website/minecraft-shaders-polymc/)
- [Signing into PolyMC](/user-guide-website/polymc-sign-in/)

